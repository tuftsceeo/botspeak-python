import RPi.GPIO as GPIO
import inspect
import time

BRICK_PI = False
try: 
    import bp
    BRICK_PI = True
except ImportError:
    BRICK_PI = False

#botspeak environment variables
BOTSPEAK_DIO      =  {3 : GPIO.LOW, 
                      5 : GPIO.LOW,
                      7 : GPIO.LOW,
                      8 : GPIO.LOW,
                      10: GPIO.LOW,
                      11: GPIO.LOW,
                      12: GPIO.LOW,
                      13: GPIO.LOW,
                      15: GPIO.LOW,
                      16: GPIO.LOW,
                      18: GPIO.LOW,
                      19: GPIO.LOW,
                      21: GPIO.LOW,
                      22: GPIO.LOW,
                      23: GPIO.LOW,
                      24: GPIO.LOW,
                      26: GPIO.LOW}
BOTSPEAK_VARS     = {'DIO' : BOTSPEAK_DIO,
                     'AI'  : {},
                     'PWM' : {},
                     'TMR' : {},
                     'SCPT': {},
                     'LBLS': {},
                     'VER' : '1.0'}
BOTSPEAK_RESERVED = ['DIO','AO','TMR','VER', 'LBL']
BOTSPEAK_FUNCS    = {}
SCPTMODE          = False
PRGM_CTR          = 0

#initializes all GPIO pins to OUT
def init_pins():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    for pin in BOTSPEAK_DIO:
            GPIO.setup(pin, GPIO.OUT)

#checks if string value is a number
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

#parses function arguments from input
def parseArgs(args):
    if len(args) == 2 and '[' and ']' in args[0]:
        temp  = [0,0,0]
        brkt1 = args[0].find('[') + 1
        brkt2 = args[0].find(']')
        index = args[0][brkt1:brkt2]
        label = args[0][0:brkt1 - 1]
        val   = args[1]
        temp[0] = label
        temp[1] = int(val) if is_number(val) else val
        temp[2] = int(index)
        return temp
    elif len(args) == 2:
        args[0] = int(args[0]) if is_number(args[0]) else args[0]
        args[1] = int(args[1]) if is_number(args[1]) else args[1]
        return args
    elif len(args) == 1 and '[' and ']' in args[0]:
        temp = [0,0]
        brkt1 = args[0].find('[') + 1
        brkt2 = args[0].find(']')
        index = args[0][brkt1:brkt2]
        label = args[0][0:brkt1 - 1]
        temp[0] = label
        temp[1] = int(index)
        return temp
    elif len(args) == 1:
        args[0] = int(args[0]) if is_number(args[0]) else args[0]
        return args

#parses string input for proper syntax
def parseInput(input):
    global SCPTMODE
    returnval = '-1'

#    print "input: " + input

    input.strip() #trim the input
    commandData = input.split(' ')

    print "commandData[0]: " + commandData[0]
    print "SCPTMODE: " + str(SCPTMODE)

    if commandData[0] in BOTSPEAK_FUNCS:

        if(commandData[0] == 'IF'):
            args = {}
            paren1 = input.find('(')
            paren2 = input.find(')')
            vals = input[paren1 + 1:paren2].split(' ')
            goto_val = input[len(input)-1]
            args[0] = vals[0]
            args[1] = vals[2]
            args[2] = vals[1]
            args[3] = goto_val
        else:
            try:
                args = commandData[1].split(',')
                if len(args) > 0:
                    args = parseArgs(args)
            except:
                args = ''
            
        print "cmd:" + commandData[0]
        print "args:" + str(args)
        
        func = BOTSPEAK_FUNCS[commandData[0]]

        if commandData[0] != 'ENDSCRIPT':
            params = inspect.getargspec(func)[0]
        
        try:

            if commandData[0] == 'SCRIPT' and not SCPTMODE:
                SCPTMODE = True
                if len(BOTSPEAK_VARS['SCPT']) > 0:
                    BOTSPEAK_VARS['SCPT'] = {}
                if len(BOTSPEAK_VARS['LBLS']) > 0:
                    BOTSPEAK_VARS['LBLS'] = {}
                returnval = 'SCRIPT'
            elif commandData[0] == 'ENDSCRIPT' and SCPTMODE:
                SCPTMODE  = False
                returnval = 'ENDSCRIPT'
            elif commandData[0] == 'ENDSCRIPT' and not SCPTMODE:
                returnval = 'Call SCRIPT before calling ENDSCRIPT'
            elif commandData[0] != 'SCRIPT' and SCPTMODE:
                if commandData[0] == 'RUN':
                    returnval = "Cannot run script while in script mode"
                else:
                    returnval = script(input)
            elif commandData[0] != 'SCRIPT' and not SCPTMODE:
                returnval = func(*args)

        except TypeError as err:
            returnval = str(err)
                
        return str(returnval)
        
    else:
        return '-1'

#checks if var is bound to the global var environment
def getvar(var, index=None):

    if isinstance(var, basestring):

        #check to see if var exists in var environment
        try:
            var = BOTSPEAK_VARS[var]
        except KeyError:
            return '"' + var '" is undefined.'
        
        #check for dictionary types
        if isinstance(var,dict):
            if isinstance(index, int):
                try:
                    if var == BOTSPEAK_VARS['AI'] and BRICK_PI:
                        var[index] = bp.ReadAnalog(index)
                        var = var[index]
                    else:
                        var = var[index]
                except KeyError:
                    return "Index out of range"

                return var

        #return the value of the name
        return var
            
    #var is an integer literal
    elif isinstance(var, int):
        return var

    #otherwise we were given some weird variable type
    else:
        return 'Invalid type'
    
BOTSPEAK_FUNCS['GET'] = getvar

#sets var to val. var accepts array types as well
def setvar(var, val, index=None):
    
    #if var is a label, check vars environment
    if isinstance(var, basestring):

        #check to see if var exists in var environment
        try:
            BOTSPEAK_VARS[var]
        #otherwise create new a one
        except KeyError:
            BOTSPEAK_VARS[var] = val
        
        #check for DIO
        if isinstance(BOTSPEAK_VARS[var], dict):
            if isinstance(index, int):
                try:
                    BOTSPEAK_VARS[var][index] = val
                    if var == 'DIO':
                        GPIO.output(index, val)
                    
                    if var == 'PWM' and BRICK_PI:
                        if index == 0:
                            bp.SetMotor(index,int(val))
                        elif index == 1:
                            bp.SetMotor(index, int(val))
                        elif index == 2:
                            bp.SetMotor(index, int(val))
                        return val

                except KeyError:
                    return  "Invalid index: '" + index + "'"

        #set var to val
        elif isinstance(val, int):
            BOTSPEAK_VARS[var] = val

        return BOTSPEAK_VARS[var]
            
    #otherwise we were given some weird variable type
    else:
        return "Invalid var type"

BOTSPEAK_FUNCS['SET'] = setvar

def run():
    global PRGM_CTR
    script = BOTSPEAK_VARS['SCPT']    
    if script is not None:
        scpt_len = len(BOTSPEAK_VARS['SCPT'])
        while PRGM_CTR < scpt_len:
            print "prgm_ctr:" + str(PRGM_CTR)
            input = BOTSPEAK_VARS['SCPT'][PRGM_CTR]
            returnval = parseInput(input)
            print returnval
            PRGM_CTR += 1
                       
        PRGM_CTR = 0
        return 'RUN'
    else:
        return -1
    
BOTSPEAK_FUNCS['RUN'] = run

def goto(inst):
    global PRGM_CTR
    if(isinstance(inst, basestring)):
        PRGM_CTR = BOTSPEAK_VARS['LBLS'][inst]
    elif (isinstance(inst, int)):
        if inst >= 0:
            PRGM_CTR = inst - 1
    return PRGM_CTR

BOTSPEAK_FUNCS['GOTO'] = goto

def script(inst):
    try:
        print(inst)
        next = len(BOTSPEAK_VARS['SCPT'])
        if(inst.find('LBL') != -1):
            lbl_inf = inst.split(' ')
            BOTSPEAK_VARS['LBLS'][lbl_inf[1]] = next

        BOTSPEAK_VARS['SCPT'][next] = inst
        return inst
    except:
        return -1

BOTSPEAK_FUNCS['SCRIPT'] = script
BOTSPEAK_FUNCS['ENDSCRIPT'] = None

def lbl(lbl):
    return lbl

BOTSPEAK_FUNCS['LBL'] = lbl

def if_goto(var1, var2, comp, goto_val):
    val1 = getvar(var1)
    val2 = getvar(var2)
    goto = False;

    if comp == '<':
        goto = (val1 < val2)
    elif comp == '>':
        goto = (val1 > val2)
    elif comp == '=':
        goto = (val1 == val2)
    elif comp == '<=':
        goto = (val1 <= val2)
    elif comp == '>=':
        goto = (val1 >= val2)
    elif comp == '!=':
        goto = (val1 != val2)

    if(goto):
        goto(goto_val)

BOTSPEAK_FUNCS['IF'] = if_goto

def add(op1, op2):

    val1 = getvar(op1)
    val2 = getvar(op2)
    val1 += val2
    setvar(op1, val1)
    return val1

BOTSPEAK_FUNCS['ADD'] = add

def sub(op1, op2):
    val1 = getvar(op1)
    val2 = getvar(op2)
    val1 -= val2
    setvar(op1, val1)
    return val1

BOTSPEAK_FUNCS['SUB'] = sub

def div(op1, op2):
    #divide by zero error
    if op2 == 0:
        return -1

    val1 = getvar(op1)
    val2 = getvar(op2)
    val1 /= val2
    setvar(op1, val1)
    return val1

BOTSPEAK_FUNCS['DIV'] = div

def wait(n):
    time.sleep(n)

BOTSPEAK_FUNCS['WAIT'] = wait
